#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php /var/www/phpcs.phar "$@"
fi

exec "$@"