FROM mtoy/php:7.2

RUN apt-get update \
    && apt-get install -y gnupg2 \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && apt-get install -y \
        git \
        zip \
        unzip \
        --no-install-recommends \
    && curl -o /var/www/phpcs.phar -OL https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar \
    && rm -r /var/lib/apt/lists/*

ADD entry.sh /entry.sh

RUN chmod 777 /entry.sh

ENTRYPOINT ["/entry.sh"]

CMD ["-h"]